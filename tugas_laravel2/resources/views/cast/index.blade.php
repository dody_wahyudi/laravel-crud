@extends('layout.master')

@section('judul')
List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary my-2">Tambah Berita</a>
<div class="row">
    @forelse($cast as $item)
        <div class="col-4">
            <div class="card">
            <img class="card-img-top" src="..." alt="Gambar Tidak Ada">
                <div class="card-body">
                    <h3 class="">Nama : {{$item->nama}}</h3>
                    <p class="card-text">Umur : {{$item->umur}}</p>
                    <p class="card-text"> {{ Str::limit($item->bio, 50) }}</p>   
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('Delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </div>
            </div>
        </div> 
    @empty
        <h1>Data Cast Masih Kosong</h1>
    @endforelse
</div>

@endsection