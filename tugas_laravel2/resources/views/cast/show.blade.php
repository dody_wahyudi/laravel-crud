@extends('layout.master')

@section('judul')
Detail Kategori {{$cast->nama}}
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary">Kembali</a>
@endsection