@extends('layout.master')

@section('judul')
Halaman Index
@endsection


@section('content')
	<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
		@csrf
		<label for="">First Name :</label><br><br>
		<input type="text" name="fname" id=""><br><br>
		<label for="">Last Name :</label><br><br>
		<input type="text" name="lname" id=""><br><br>
		<label for="">Gender</label><br><br>
		<input type="radio" name="gender" id="" value="male">Male<br>
		<input type="radio" name="gender" id="" value="female">Female<br>
		<input type="radio" name="gender" id="" value="orther">Other<br><br>
		<label for="">Nationally</label><br><br>
		<select name="" id="">
			<option value="indonesia">Indonesia</option>
			<option value="english">English</option>
			<option value="other">Other</option><br><br>
		</select>
		<label for="">Language Spoken</label><br><br>
		<input type="checkbox" name="language" id="indonesia" value="Bahasa Indonesia">
		<label for="indonesia">Indonesia</label><br/>
		<input type="checkbox" name="language" id="inggris" value="English">
		<label for="inggris">English</label><br/>
		<input type="checkbox" name="language" id="other" value="Other">
		<label for="other">Other</label><br/>
		<p>Bio :</p>
		<textarea name="" id="" cols="30" rows="10"></textarea><br/>
		<input type="submit" name="" id="" value="kirim"/>
	</form>
@endsection